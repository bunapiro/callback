#include <stdio.h>
#include <stdlib.h>
#include "../../include/local.h"


#define MAX_CALC_FUNCTION_NUM   10

/* 関数ポインタの型定義 */
typedef int (*P_CALC_FUNCTION_T)( int, int );

PRIVATE P_CALC_FUNCTION_T m_aCalcFunction[ MAX_CALC_FUNCTION_NUM ];
PRIVATE UINT m_RegisteredCalcFuncNum = 0;


PRIVATE ERROR_T RegisterCalcFunction( P_CALC_FUNCTION_T pCalcFunc )
{
    if ( m_RegisteredCalcFuncNum >= MAX_CALC_FUNCTION_NUM )
    {
        PRINT("MAX_CALC_FUNCTION_NUM OVER !!");
        return FAILURE;
    }

    m_aCalcFunction[ m_RegisteredCalcFuncNum ] = pCalcFunc;
    m_RegisteredCalcFuncNum++;

    return SUCCESS;
}


PRIVATE int CalcAdd( int x, int y )
{
    return x + y;
}

PRIVATE int CalcSub( int x, int y )
{
    return x - y;
}

PRIVATE int CalcMulti( int x, int y )
{
    return x * y;
}

PRIVATE int CalcDiv( int x, int y )
{
    if ( y == 0 )
    {
        return x;
    }

    return x / y;
}


PUBLIC int main( int argc, char **argv )
{
    UINT i;
    int Result;

    /* 関数の登録 */
    RegisterCalcFunction( CalcAdd );
    RegisterCalcFunction( CalcSub );
    RegisterCalcFunction( CalcMulti );
    RegisterCalcFunction( CalcDiv );


    /* 登録された関数の呼び出し */
    for ( i = 0; i < m_RegisteredCalcFuncNum; i++ )
    {
        Result = m_aCalcFunction[i]( 10, 5 );
        PRINT_ARG("Result = %d" ,Result );
    }


    return EXIT_SUCCESS;
}


